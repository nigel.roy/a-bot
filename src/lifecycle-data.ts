import { getProperty, setProperty } from 'dot-prop';

type LifecycleData = { [key: string]: LifecycleData | string };

class LifecycleDataManager {
  private rawData: LifecycleData = {};
  private readbackData: LifecycleData = {};

  constructor() {}

  get data() {
    return {
      raw: this.rawData,
      readback: this.readbackData,
    };
  }

  setData(key: string, parentPath: string | undefined, raw: string, rb?: string | null) {
    let path = key;
    if (parentPath) {
      path = `${parentPath}.${key}`;
    }

    const readback = rb || raw;

    setProperty(this.rawData, path, raw);
    setProperty(this.readbackData, path, readback);
  }

  getReadbackData(keys: string[], parentPath: string | undefined) {
    const readbackData: Record<string, string> = {};

    const paths = keys.map((key) => (parentPath ? `${parentPath}.${key}` : key));
    for (const path of paths) {
      const val = getProperty(this.readbackData, path);
      setProperty(readbackData, path, val);
    }

    return readbackData;
  }
}

export const lifecycleDataManager = new LifecycleDataManager();
