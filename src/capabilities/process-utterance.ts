import { handleRequestAgent } from './request-agent.js';

export function processUtteranceForCapabilities(utterance: string) {
  const intent = inferIntent(utterance);
  handleIntent(intent);
}

function inferIntent(utterance: string) {
  // basic for now
  if (utterance === 'agent') {
    return 'RequestAgent';
  }

  return 'ProvideInfo';
}

function handleIntent(intent: string) {
  // basic for now
  if (intent === 'RequestAgent') {
    handleRequestAgent();
  }
}
