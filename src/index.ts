import 'dotenv/config';

import def from './bot-definition.json' assert { type: 'json' };
import { FlowState, type FlowStateConfig } from './states/flow/state.js';

const mainFlow = new FlowState('', undefined, def as FlowStateConfig);
mainFlow.do();
