import type { BaseLlmClient } from './base.js';
import { OpenAiClient } from './openai.js';

let client: BaseLlmClient;
const vendor = process.env.LLM_VENDOR;

switch (vendor) {
  case 'openai':
    client = new OpenAiClient();
    break;
  default:
    throw new Error(`unsupported LLM vendor ${vendor}`);
}

export default client;
