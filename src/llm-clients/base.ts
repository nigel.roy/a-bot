export abstract class BaseLlmClient {
  constructor() {}

  abstract complete(prompt: string, fallback: string): Promise<string>;
}
