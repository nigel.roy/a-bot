import OpenAI from 'openai';
import type { ChatCompletionMessageParam } from 'openai/resources/index.mjs';

import { BaseLlmClient } from './base.js';

export class OpenAiClient extends BaseLlmClient {
  private client;

  constructor() {
    super();

    this.client = new OpenAI({
      apiKey: process.env.OPENAI_API_KEY,
      maxRetries: 2,
    });
  }

  async complete(prompt: string, fallback: string) {
    const messages: ChatCompletionMessageParam[] = [{ role: 'system', content: prompt }];

    return await this.client.chat.completions
      .create({ model: 'gpt-3.5-turbo', messages })
      .then((completion) => completion.choices[0].message.content || fallback)
      .catch(() => fallback);
  }
}
