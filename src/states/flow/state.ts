import { BaseState, type BaseStateConfig } from '../base.js';
import { CollectBasicState } from '../collect/basic/state.js';
import { CollectPhoneState } from '../collect/phone/state.js';
import { SayState } from '../say/state.js';
import type { StateConfig } from '../types.js';

export type FlowStateConfig = BaseStateConfig & {
  kind: 'flow';
  entrypoint: 'string';
  states: Record<string, StateConfig>;
};

export class FlowState extends BaseState {
  constructor(
    name: string,
    parentPath: string | undefined,
    private config: FlowStateConfig,
  ) {
    super(name, parentPath);
  }

  async do(stateName?: string) {
    const startState = stateName || this.config.entrypoint;
    const stateConfig = this.config.states[startState];

    const state = this.generateState(startState, stateConfig);

    await state.do();

    const nextState = stateConfig.next;
    if (nextState) {
      await this.do(nextState);
    }
  }

  private generateState(name: string, config: StateConfig) {
    switch (config.kind) {
      case 'say':
        return new SayState(name, this.parentPath, config);
      case 'collect-basic':
        return new CollectBasicState(name, this.parentPath, config);
      case 'collect-phone':
        return new CollectPhoneState(name, this.parentPath, config);
      case 'flow': {
        const newParentPath = this.parentPath ? `${this.parentPath}.${name}` : name;
        return new FlowState(name, newParentPath, config);
      }
    }
  }
}
