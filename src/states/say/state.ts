import { BaseState, type BaseStateConfig } from '../base.js';

export type SayStateConfig = BaseStateConfig & {
  kind: 'say';
  phrase: string;
};

export class SayState extends BaseState {
  constructor(
    name: string,
    parentPath: string | undefined,
    private config: SayStateConfig,
  ) {
    super(name, parentPath);
  }

  async do() {
    await this.speak(this.config.phrase);
  }
}
