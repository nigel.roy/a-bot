import type { CollectStateConfig } from './collect/types.js';
import type { FlowStateConfig } from './flow/state.js';
import type { SayStateConfig } from './say/state.js';

export type StateConfig = SayStateConfig | FlowStateConfig | CollectStateConfig;
