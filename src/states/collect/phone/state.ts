import dedent from 'dedent';
import Handlebars from 'handlebars';

import { lifecycleDataManager } from '../../../lifecycle-data.js';
import llmClient from '../../../llm-clients/index.js';
import { BaseState, type BaseStateConfig } from '../../base.js';

export type CollectPhoneStateConfig = BaseStateConfig & {
  kind: 'collect-phone';
  question: string;
};

export class CollectPhoneState extends BaseState {
  constructor(
    name: string,
    parentPath: string | undefined,
    private config: CollectPhoneStateConfig,
  ) {
    super(name, parentPath);
  }

  async do() {
    await this.speak(this.config.question);

    const utterance = await this.listen();

    const promptTemplateFn = Handlebars.compile(this.prompt);
    const prompt = promptTemplateFn({ question: this.config.question, utterance });

    const msg = await llmClient.complete(prompt, utterance);

    lifecycleDataManager.setData(this.name, this.parentPath, msg, this.getReadback(msg));
  }

  private get prompt() {
    return dedent`
      You are a bot responsible for retrieving a phone number from the given user input. 

      You will be given the question supplied to the user as well as the user input.

      If you can discern a phone number that is 10, 11 or 12 digits long from the user 
      input, return exactly that phone number as 10, 11 or 12 numeric digits like 
      4166710707 or +14166710707.

      Discard any characters in the user input other than the numbers 1 to 10 in either 
      numeric or alphabetic form.
      
      Discard all formatting other than the plus + symbol.

      If you cannot discern any phone number, return null.

      Given below is the question supplied to the user and the user input:

      Question: {{ question }}
      User Input: {{ utterance }}
    `;
  }

  private getReadback(msg: string) {
    if (msg === 'null') {
      return null;
    }

    const sepIdx = msg.length - 10;

    const extension = msg.slice(0, sepIdx);
    const phoneNumber = msg.slice(sepIdx);

    const extensionReadback = extension.split('').join(' ');

    const phoneNumberDigits = phoneNumber.split('');
    const phoneNumberDigitGroups = [
      phoneNumberDigits.slice(0, 3),
      phoneNumberDigits.slice(3, 6),
      phoneNumberDigits.slice(6),
    ];

    const phoneNumberDigitGroupsReadback = phoneNumberDigitGroups.map((group) => group.join(' '));
    const phoneNumberReadback = phoneNumberDigitGroupsReadback.join(', ');

    let phoneReadback = '';

    if (extensionReadback) {
      phoneReadback += extensionReadback + ', ';
    }

    phoneReadback += phoneNumberReadback;

    return phoneReadback;
  }
}
