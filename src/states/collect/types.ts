import type { CollectBasicStateConfig } from './basic/state.js';
import type { CollectPhoneStateConfig } from './phone/state.js';

export type CollectStateConfig = CollectBasicStateConfig | CollectPhoneStateConfig;
