import { lifecycleDataManager } from '../../../lifecycle-data.js';
import { BaseState, type BaseStateConfig } from '../../base.js';

export type CollectBasicStateConfig = BaseStateConfig & {
  kind: 'collect-basic';
  question: string;
};

export class CollectBasicState extends BaseState {
  constructor(
    name: string,
    parentPath: string | undefined,
    private config: CollectBasicStateConfig,
  ) {
    super(name, parentPath);
  }

  async do() {
    await this.speak(this.config.question);

    const utterance = await this.listen();

    await lifecycleDataManager.setData(this.name, this.parentPath, utterance);
  }
}
