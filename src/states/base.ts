import { input } from '@inquirer/prompts';
import Handlebars from 'handlebars';
import say from 'say';

import { processUtteranceForCapabilities } from '../capabilities/process-utterance.js';
import { lifecycleDataManager } from '../lifecycle-data.js';

export type BaseStateConfig = {
  next?: string;
};

export abstract class BaseState {
  constructor(
    protected name: string,
    protected parentPath: string | undefined,
  ) {}

  abstract do(): Promise<void>;

  protected async speak(templatePhrase: string) {
    const templateFn = Handlebars.compile(templatePhrase);
    const templateVars = getTemplateVariables(templatePhrase);

    let readbackData: Record<string, string> | undefined;
    if (templateVars.length > 0) {
      readbackData = lifecycleDataManager.getReadbackData(templateVars, this.parentPath);
    }

    const phrase = templateFn(readbackData);

    await new Promise((resolve, reject) => {
      say.speak(phrase, undefined, undefined, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(undefined);
        }
      });
    });
  }

  protected async listen() {
    const utterance = await input({ message: '' });

    processUtteranceForCapabilities(utterance);

    return utterance;
  }
}

function getTemplateVariables(str: string) {
  const ast = Handlebars.parseWithoutProcessing(str);

  const mustacheStatements = ast.body.filter(
    (statement): statement is hbs.AST.MustacheStatement => statement.type === 'MustacheStatement',
  );
  const vars = mustacheStatements
    .map((statement) => {
      const param = statement.params[0];
      if (param && 'original' in param) {
        return param.original;
      }

      const path = statement.path;
      if (path && 'original' in path) {
        return path.original;
      }

      return undefined;
    })
    .filter((statement): statement is string => statement !== undefined);

  return vars;
}
